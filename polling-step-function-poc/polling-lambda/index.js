const AWS = require('aws-sdk');

const S3 = new AWS.S3();

exports.handler = async (event, context, done) => {
  const outputFilePath = `${event.id}/${event.id}.output.csv`;
  console.log(`Event for Job: ${event.id} - Looking for output file ${outputFilePath} in bucket ${event.bucketName}...`);

  const waitInfo = event.waitInfo;
  waitInfo.count += 1;

  let foundFile = false;
  try {
    const params = {
        Bucket: event.bucketName,
        Key: outputFilePath
    }
    const headObjectResponse = await S3.headObject(params).promise();
    waitInfo.found = true;
  }
  catch (exception) {
    if (waitInfo.count >= waitInfo.max) {
      throw new Error("Tired of waiting");
    }
  }

  return waitInfo;
};
