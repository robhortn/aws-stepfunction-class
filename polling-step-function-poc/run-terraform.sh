#!/usr/bin/env bash

if [ -z "$AWS_PROFILE" ]
then
      echo "AWS Profile not set"
      exit 1
fi

TF_ACTION=${1:-"plan"}

# clean up local terraform state
rm -rf .terraform

# AWS Region
export AWS_DEFAULT_REGION=us-east-1

cd polling-lambda/
rm polling-lambda.zip
zip ./polling-lambda.zip -r index.js

cd ..
terraform init
terraform $TF_ACTION
