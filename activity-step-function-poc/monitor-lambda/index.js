const AWS = require('aws-sdk');

const stepFunctionApi = new AWS.StepFunctions();
const S3 = new AWS.S3();

exports.handler = async (event, context, done) => {

  // respond to 's3 notification of 123.product-output.csv'
  if (event.Records.length !== 1) {
    console.log(`A s3 event was received that contained ${event.Records.length} records. Ignoring the whole event.`);
    return;
  }

  const fileEventRecord = event.Records[0];
  if (!fileEventRecord.eventName.startsWith("ObjectCreated")) {
    console.log(`Event of type ${fileEventRecord.eventName} was ignored by the file event processor`);
    return;
  }

  const monitorBucketName = fileEventRecord.s3.bucket.name;
  const fileKey = fileEventRecord.s3.object.key;
  const pathTokens = fileKey.split("/");
  if (pathTokens.length !== 2) {
    console.log(`File key \`${fileKey}\` didn't match expectations. Ignoring event.`);
    return;
  }

  // bucketName, in this case, is the bucket where we can also find the task state file
  // fileKey, in this case, should be id/output.csv
  // look up matching task state file

  const id = pathTokens[0];
  const taskStateFileParams = {
    Bucket: process.env.METADATA_BUCKET,
    Key: `${id}/task-state.json`
  }
  console.log(`Pulling task state file from ${JSON.stringify(taskStateFileParams)}...`);
  const getObjectResponse = await S3.getObject(taskStateFileParams).promise();
  const state = JSON.parse(getObjectResponse.Body.toString('utf-8'));

  // unblock activity
  console.log(`Retrieved task state file contents: ${JSON.stringify(state)}`);
  const taskSuccessParams = {
    output: JSON.stringify({
      monitorBucketName,
      fileKey
    }),
    taskToken: state.taskToken
  }
  await stepFunctionApi.sendTaskSuccess(taskSuccessParams).promise();
  console.log("Successfully marked product task complete.");
};
