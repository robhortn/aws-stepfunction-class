const AWS = require('aws-sdk');

const stepFunctionApi = new AWS.StepFunctions();
const S3 = new AWS.S3();

exports.handler = async (event, context, done) => {

  /*
  data.input:
  "id",
  "metadataBucketName"
  */

  const taskParams = {
    activityArn: process.env.ACTIVITY_ARN
  }

  console.log(`Checking for waiting activity: ${taskParams.activityArn}`);
  const data = await stepFunctionApi.getActivityTask(taskParams).promise();
  if (data === null || data.taskToken === undefined) {
    console.log("No activity instances waiting. Going back to sleep...");
  }
  else {
    // POC: This can be done in a loop, to pull as many instances off the activity
    // queue as are ready.
    console.log(`Retrieved waiting activity instance ${JSON.stringify(data)}`);

    const input = JSON.parse(data.input);
    const taskFileParams = {
      Bucket: input.metadataBucketName,
      Key: `${input.id}/task-state.json`,
      Body: JSON.stringify({
        taskToken: data.taskToken
      })
    };

    console.log(`Writing task state file to ${taskFileParams.Bucket}/${taskFileParams.Key}...`);
    await S3.putObject(taskFileParams).promise();
  }

  // POC: Do whatever your activity needs to do, or trigger something else
};
